from django.contrib import admin
from django.urls import include, path
from xml.dom.minidom import Document
from . import views
from members import pedido

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.contrib.staticfiles.urls import static



urlpatterns = [
    path('', views.home, name='home'),
    #path('bibliotecario/libros/', include('members.urls')),
    #path('bibliotecario/grafico/', include('members.urls')),
    path('biblioteca/', include('members.urls')),
    #path('members/', include('members.urls')),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),


]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
