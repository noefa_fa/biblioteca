from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import Usuario, Libro, Autor, GeneroLiterario, Editorial, Prestamo, Ejemplar
from datetime import datetime

###
class UsuarioCreationForm(UserCreationForm):
    class Meta:
        model = Usuario
        fields = ("username", "email")

class RegisterForm(UserCreationForm):
    username = forms.CharField(label = 'Usuario',required=True,)
    first_name = forms.CharField(label = 'Nombre',required=True,)
    last_name = forms.CharField(label = 'Apellido',required=True,)
    email = forms.EmailField(label= 'Email',required=True,)
    password1 = forms.CharField(label = "Contraseña",required=True, widget=forms.PasswordInput)
    password2 = forms.CharField(label = "Confirmar Contraseña",required=True, widget=forms.PasswordInput)
    class Meta:
        model = Usuario
        fields = ("username", "first_name", "last_name", "email", "password1", "password2")


class UsuarioChangeForm(UserChangeForm):
    class Meta:
        model = Usuario
        fields = ("username", "email")

class LibroForm(forms.ModelForm):
    class Meta:
        model = Libro
        fields = '__all__'

class AutorForm(forms.ModelForm):
    class Meta:
        model = Autor
        fields = ("nombre", "apellido", "pseudonimo")

        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'apellido': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'pseudonimo': forms.TextInput(attrs={'class': 'form-control', 'required': False}),
        }


class GeneroLiterarioForm(forms.ModelForm):
    class Meta:
        model = GeneroLiterario
        fields = ("nombre",)

        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
        }

class EditorialForm(forms.ModelForm):
    class Meta:
        model = Editorial
        fields = ("nombre",)

        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
        }

class EjemplarForm(forms.ModelForm):
    class Meta:
        model = Ejemplar
        fields = ("libro", "disponible")

        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            #'titulo': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'libro': forms.Select(attrs={'class': 'form-control', 'required': True}),
            'disponible': forms.NullBooleanSelect(attrs={'class': 'form-control', 'required': True, 'default': True}),
        }


class PrestamoForm(forms.ModelForm):
    class Meta:
        model = Prestamo
        fields = ("Fecha_devolucion", "Fecha_entrega", "Limite_entrega", "Limite_devolucion", "ejemplar", "lector", "bibliotecario_entrega", "bibliotecario_recibe")

        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'Fecha_devolucion': forms.DateInput(attrs={'type': 'date', 'class': 'form-control', 'required': False}),
            'Fecha_entrega': forms.DateInput(attrs={'type': 'date', 'class': 'form-control', 'required': False}),
            'Limite_entrega': forms.DateInput(attrs={'type': 'date', 'class': 'form-control', 'required': True}),
            'Limite_devolucion': forms.DateInput(attrs={'type': 'date', 'class': 'form-control', 'required': False, 'default': datetime.now}),
            'ejemplar': forms.Select(attrs={'class': 'form-control', 'required': True}),
            'lector': forms.Select(attrs={'class': 'form-control', 'required': True}),
            'bibliotecario_entrega': forms.Select(attrs={'class': 'form-control', 'required': False}),
            'bibliotecario_recibe': forms.Select(attrs={'class': 'form-control', 'required': False}),
        }
        print(datetime.now)
    def __init__(self, request, instance):
        super(PrestamoForm, self).__init__(request, instance)
        #print(user, 'aaa')
        self.fields['lector'].queryset = Usuario.objects.filter(groups=1)
        self.fields['bibliotecario_entrega'].queryset = Usuario.objects.filter(groups=2)
        self.fields['bibliotecario_recibe'].queryset = Usuario.objects.filter(groups=2)
