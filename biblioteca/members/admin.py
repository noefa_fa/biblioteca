from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
# Register your models here.
from .models import Autor, Libro, GeneroLiterario, Editorial, Ejemplar, Usuario, Prestamo
from django.contrib import admin

from .forms import UsuarioCreationForm, UsuarioChangeForm


class UsuarioAdmin(UserAdmin):
    add_form = UsuarioCreationForm
    form = UsuarioChangeForm
    model = Usuario
    list_display = ["email", "username", "first_name", "last_name"]

class LibroAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'genero', 'editorial')
    list_filter = ('genero', 'editorial')
    search_fields = ('titulo',)



# Register your models here.
admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Libro, LibroAdmin)

admin.site.register(Autor)
admin.site.register(GeneroLiterario)
admin.site.register(Editorial)
admin.site.register(Ejemplar)
admin.site.register(Prestamo)
