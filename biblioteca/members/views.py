from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.template import loader
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.contrib import messages
from django.db.models import Count, Sum
from django.db import models
import json
from .pedido import Pedido
from .models import Usuario, Libro, Autor, GeneroLiterario, Editorial, Ejemplar, Prestamo
from .forms import LibroForm, AutorForm, GeneroLiterarioForm, EditorialForm, PrestamoForm, EjemplarForm, RegisterForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import Group

import datetime

import csv

def Registro(request):
    # Cargamos los fields desde forms.py
    data = {
        'form': RegisterForm()
    }
    # Metodo de accion por POST
    if request.method == 'POST':
        # Asignamos el formulario con los datos ingresados
        formulario  = RegisterForm(data=request.POST)
        # Fomulario debe estar lleno
        if formulario.is_valid():
            # Guardamos al usuario con los datos del formulario
            user = formulario.save()
            my_group = Group.objects.get(name='Lector')
            my_group.user_set.add(user)
            login(request,user)
            if user is not None:
                # Mensaje de bienvenida
                messages.success(request, "Te has registrado correctamente")
                # Mostramos mensaje por terminal
                print("El {} ha sido agregado al grupo {}".format(user, my_group))
                return redirect(to = "home")

        data["form"] = formulario
    # cargamos plantilla de registro
    return render(request,"registration/registro.html", data)


# Decorador para verificar grupo.
def es_bibliotecario(user):
    lectores = user.groups.filter(name="Lector")
    # usuario autenticado?
    if (user.is_authenticated):
        # usuario pertenece al grupo Bibliotecario?
        return user.groups.filter(name='Bibliotecario').exists()
    else:
        return False

# Libros

# Create your views here.
@user_passes_test(es_bibliotecario)
def index(request):
    template = loader.get_template('bibliotecario/libros/index.html')

    libros = Libro.objects.all()
    ejem=[]
    for i in libros:
        ejemplares = Ejemplar.objects.filter(libro=i, disponible=True)
        ejem.append({'libro': i, 'ejemplares': ejemplares})
    context = {
        'libros': libros,
        'ejem': ejem,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))

@user_passes_test(es_bibliotecario)
def index_autor(request):
    template = loader.get_template('bibliotecario/autor/index.html')

    autores = Autor.objects.all()
    #ejemplares = Ejemplar.objects.filter(libro=libros)

    context = {
        'autores': autores,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))


@user_passes_test(es_bibliotecario)
def add(request):
    if (request.method == 'GET'):
        form = LibroForm()
        # muesta usarios de esos grupos.
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        #queryset = Usuario.objects.filter(groups__name__in=['bibliotecario', 'Director'])

        # mostrar formulario.
        template = loader.get_template('bibliotecario/libros/add.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        #form = LibroForm(request.POST)
        form = LibroForm(request.POST or None, request.FILES or None)
        #print(form)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")
            # b = Blog(name='Beatles Blog', tagline='All the latest Beatles news.')
            libro_agregado = Libro.objects.all().values()
            ids = 0
            for i in libro_agregado:
                if i['id'] > ids:
                    ids = i['id']
            E = Ejemplar(libro_id=ids)
            E.save()
            print(ids)

        else:
            messages.warning(request, 'No se ingresó')
        return redirect('index')
    return render(request, template, {'form': form})


@user_passes_test(es_bibliotecario)
def add_autor(request):
    if (request.method == 'GET'):
        form = AutorForm()
        # muesta usarios de esos grupos.
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        #queryset = Usuario.objects.filter(groups__name__in=['bibliotecario', 'Director'])

        # mostrar formulario.
        template = loader.get_template('bibliotecario/autor/add.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = AutorForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('index_autor')



@user_passes_test(es_bibliotecario)
def delete(request, id):
    libro = Libro.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('bibliotecario/libros/delete.html')

        context = {
            'settings': settings,
            'libro': libro,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # elimina la publicación.
        libro.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('index')

@user_passes_test(es_bibliotecario)
def delete_autor(request, id):
    autor = Autor.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('bibliotecario/autor/delete.html')

        context = {
            'settings': settings,
            'autor': autor,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # elimina la publicación.
        autor.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('index_autor')

@user_passes_test(es_bibliotecario)
def update(request, id):
    libro = Libro.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario con datos.
        form = LibroForm(instance=libro)
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        template = loader.get_template('bibliotecario/libros/add.html')

        context = {
            'libro': libro,
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # actualiza datos.
        form = LibroForm(request.POST or None, request.FILES or None, instance=libro)
        if form.is_valid():
            form.save()
            messages.success(request, "Actualización realizada.")

        return redirect('index')

@user_passes_test(es_bibliotecario)
def update_autor(request, id):
    autor = Autor.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario con datos.
        form = AutorForm(instance=autor)
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        template = loader.get_template('bibliotecario/autor/add.html')

        context = {
            'autor': autor,
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # actualiza datos.
        form = AutorForm(request.POST, instance=autor)
        if form.is_valid():
            form.save()
            messages.success(request, "Actualización realizada.")

        return redirect('index_autor')

def register_request(request):
	if request.method == "POST":
		form = NewUserForm(request.POST)
		if form.is_valid():
			user = form.save()
			login(request, user)
			messages.success(request, "Registration successful." )
			return redirect("main:homepage")
		messages.error(request, "Unsuccessful registration. Invalid information.")
	form = NewUserForm()
	return render (request=request, template_name="main/register.html", context={"register_form":form})

# Lector

# Decorador para verificar grupo.
def es_lector(user):
    # usuario autenticado?
    if (user.is_authenticated):
        # usuario pertenece al grupo lector?
        return user.groups.filter(name='Lector').exists()
    else:
        return False


# Create your views here.
@user_passes_test(es_lector)
def index_lector(request):
    template = loader.get_template('lector/catalogo.html')

    libros = Libro.objects.all()
    ejem=[]
    for i in libros:
        ejemplares = Ejemplar.objects.filter(libro=i, disponible=True)
        ejem.append({'libro': i, 'ejemplares': ejemplares})
    context = {
        'libros': libros,
        'ejem': ejem,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))

## Generos

# Create your views here.
@user_passes_test(es_bibliotecario)
def index_genero(request):
    template = loader.get_template('bibliotecario/genero/index.html')

    generos = GeneroLiterario.objects.all()

    context = {
        'generos': generos,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))


@user_passes_test(es_bibliotecario)
def add_genero(request):
    if (request.method == 'GET'):
        form = GeneroLiterarioForm()
        # muesta usarios de esos grupos.
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        #queryset = Usuario.objects.filter(groups__name__in=['bibliotecario', 'Director'])

        # mostrar formulario.
        template = loader.get_template('bibliotecario/genero/add.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = GeneroLiterarioForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('index_genero')

@user_passes_test(es_bibliotecario)
def delete_genero(request, id):
    generos = GeneroLiterario.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('bibliotecario/genero/delete.html')

        context = {
            'generos': generos,
            'settings': settings,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # elimina la publicación.
        generos.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('index_genero')

@user_passes_test(es_bibliotecario)
def update_genero(request, id):
    generos = GeneroLiterario.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario con datos.
        form = GeneroLiterarioForm(instance=generos)
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        template = loader.get_template('bibliotecario/genero/add.html')

        context = {
            'generos': generos,
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # actualiza datos.
        form = GeneroLiterarioForm(request.POST, instance=generos)
        if form.is_valid():
            form.save()
            messages.success(request, "Actualización realizada.")

        return redirect('index_genero')

# Editorial

# Create your views here.
@user_passes_test(es_bibliotecario)
def index_editorial(request):
    template = loader.get_template('bibliotecario/editorial/index.html')

    editorial = Editorial.objects.all()

    context = {
        'editorial': editorial,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))


@user_passes_test(es_bibliotecario)
def add_editorial(request):
    if (request.method == 'GET'):
        form = EditorialForm()
        # muesta usarios de esos grupos.
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        #queryset = Usuario.objects.filter(groups__name__in=['bibliotecario', 'Director'])

        # mostrar formulario.
        template = loader.get_template('bibliotecario/editorial/add.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = EditorialForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('index_editorial')

@user_passes_test(es_bibliotecario)
def delete_editorial(request, id):
    editorial = Editorial.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('bibliotecario/editorial/delete.html')

        context = {
            'editorial': editorial,
            'settings': settings,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # elimina la publicación.
        generos.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('index_editorial')

@user_passes_test(es_bibliotecario)
def update_editorial(request, id):
    generos = Editorial.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario con datos.
        form = EditorialForm(instance=generos)
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        template = loader.get_template('bibliotecario/editorial/add.html')

        context = {
            'editorial': editorial,
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # actualiza datos.
        form = EditorialForm(request.POST, instance=generos)
        if form.is_valid():
            form.save()
            messages.success(request, "Actualización realizada.")

        return redirect('index_editorial')

# Prestamo

# Create your views here.
@user_passes_test(es_bibliotecario)
def index_prestamo(request):
    template = loader.get_template('bibliotecario/prestamo/index.html')

    prestamos = Prestamo.objects.all()
    user= Usuario.objects.filter(groups=2)

    context = {
        'prestamos': prestamos,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))

def actualizar_ejemplar(id_ejemplar):

    # saca el ejemplar y cambia el valor de disponible a falso
    ejemplar = Ejemplar.objects.filter(id=id_ejemplar)

    ejemplar.update(disponible=False)

@user_passes_test(es_bibliotecario)
def add_prestamo(request):
    if (request.method == 'GET'):
        form = PrestamoForm(request.GET, instance=None)
        # muesta usarios de esos grupos.
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        #queryset = Usuario.objects.filter(groups__name__in=['bibliotecario', 'Director'])

        # mostrar formulario.
        template = loader.get_template('bibliotecario/prestamo/add.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = PrestamoForm(request.POST, instance=None)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")
            # revisa los prestamos y saca el id mayor = el último en agregar
            prestamos = Prestamo.objects.all().values()
            id = 0
            for i in prestamos:
                if i['id'] > id:
                    id = i['id']
            # saca el último préstamo y el id del ejemplar que le corresponde
            prestamo = Prestamo.objects.filter(id=id)
            #print(prestamo[0].ejemplar.id)
            id_ejemplar = prestamo[0].ejemplar.id
            actualizar_ejemplar(id_ejemplar)

        return redirect('index_prestamo')

@user_passes_test(es_bibliotecario)
def delete_prestamo(request, id):
    prestamos = Prestamo.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('bibliotecario/prestamo/delete.html')

        context = {
            'prestamos': prestamos,
            'settings': settings,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # elimina la publicación.
        prestamos.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('index_prestamo')

@user_passes_test(es_bibliotecario)
def update_prestamo(request, id):
    prestamos = Prestamo.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario con datos.
        form = PrestamoForm(request.GET, instance=prestamos)
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        template = loader.get_template('bibliotecario/prestamo/add.html')

        context = {
            'prestamos': prestamos,
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # actualiza datos.
        form = PrestamoForm(request.POST, instance=prestamos)
        if form.is_valid():
            form.save()
            messages.success(request, "Actualización realizada.")

        return redirect('index_prestamo')

# ejemplar
@user_passes_test(es_bibliotecario)
def index_ejemplar(request):
    template = loader.get_template('bibliotecario/ejemplar/index.html')

    ejemplares = Ejemplar.objects.all()
    #ejemplares = Ejemplar.objects.filter(libro=libros)

    context = {
        'ejemplares': ejemplares,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))

@user_passes_test(es_bibliotecario)
def add_ejemplar(request):
    if (request.method == 'GET'):
        form = EjemplarForm()
        # muesta usarios de esos grupos.
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        #queryset = Usuario.objects.filter(groups__name__in=['bibliotecario', 'Director'])

        # mostrar formulario.
        template = loader.get_template('bibliotecario/ejemplar/add.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = EjemplarForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('index_ejemplar')

@user_passes_test(es_bibliotecario)
def update_ejemplar(request, id):
    ejemplar = Ejemplar.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario con datos.
        form = EjemplarForm(instance=ejemplar)
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        template = loader.get_template('bibliotecario/ejemplar/add.html')

        context = {
            'ejemplar': ejemplar,
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # actualiza datos.
        form = EjemplarForm(request.POST, instance=ejemplar)
        if form.is_valid():
            form.save()
            messages.success(request, "Actualización realizada.")

        return redirect('index_ejemplar')

@user_passes_test(es_bibliotecario)
def delete_ejemplar(request, id):
    ejemplar = Ejemplar.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('bibliotecario/ejemplar/delete.html')

        context = {
            'settings': settings,
            'ejemplar': ejemplar,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # elimina la publicación.
        ejemplar.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('index_ejemplar')


# home page.
def grafico(request):
    # variable de session num_visitas.
    num_visitas = request.session.get('num_visitas', 0)
    request.session['num_visitas'] = num_visitas + 1

    # default template.
    template = loader.get_template('bibliotecario/grafico/dashboard.html')

    libros = Libro.objects.all()
    # agrupa número de publicaciones por el año.
    result = (libros.values('genero').annotate(dcount=Count('genero')).order_by('genero'))
    # copia a un diccionario el rsultado.
    dataarticulos = []
    #count = 0
    for item in result:
        genre = GeneroLiterario.objects.filter(id=item['genero']).values()[0]
        dataarticulos.append({'label': genre["nombre"], 'y': item["dcount"]})

    # gráfico ejemplares.
    ejemplares = Ejemplar.objects.all()

    result2 = (ejemplares.values('libro_id').annotate(dcount=Count('libro_id')))

    dataejemplares = []
    for item in result2:
        obj = Libro.objects.filter(id=item['libro_id']).values()[0]
        dataejemplares.append({'label': obj["titulo"], 'y': item['dcount']})

    # grafico 3
    prestamos =  Prestamo.objects.all()
    result3 = (prestamos.values('Limite_entrega').annotate(dcount=Count('Limite_entrega')))
    dataprestamos = []
    for item in result3:
        #obj = Prestamo.objects.filter(id=item['']).values()[0]
        dataprestamos.append({'label': str(item['Limite_entrega']), 'y': item['dcount']})


    context = {
        'settings': settings,
        'dataarticulos': json.dumps(dataarticulos),
        'dataejemplares': json.dumps(dataejemplares),
        'dataprestamos': json.dumps(dataprestamos),
        'num_visitas': num_visitas,
    }
    #print(json.dumps(dataarticulos))

    #a_csv(dataarticulos, dataejemplares)
    return HttpResponse(template.render(context, request))

@user_passes_test(es_lector)
def historial(request):
    user = request.user
    template = loader.get_template('lector/prestamos.html')

    prestamos = Prestamo.objects.filter(lector=user)
    #user=Usuario.objects.filter(groups=2)

    context = {
        'prestamos': prestamos,
        'user': user,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))

@user_passes_test(es_lector)
def limpiar_articulo(request):
        pedido = Pedido(request)
        pedido.limpiar()
        return redirect("index_lector")


@user_passes_test(es_lector)
def agregar_articulo(request, id):
        pedido = Pedido(request)
        libro = Libro.objects.get(id=id)
        ejemplar = Ejemplar.objects.filter(libro=libro)
        add = pedido.agregar(libro, ejemplar[0])
        if add == False:
            messages.success(request, 'No se puede agregar más de un libro')
            limpiar_articulo(request)

        #print(id)
        #return redirect('')
        return redirect('index_lector')

@user_passes_test(es_lector)
def eliminar_articulo(request, id):
        pedido = Pedido(request)
        libro = Libro.objects.get(id=id)
        pedido.eliminar(libro)
        return redirect("index_lector")

@user_passes_test(es_lector)
def restar_articulo(request, id):
        pedido = Pedido(request)
        libro = Libro.objects.get(id=id)
        pedido.restar(libro)
        return redirect("index_lector")

@user_passes_test(es_lector)
def guardar_prestamo(request):
        user = request.user
        prestamos_usuario = Prestamo.objects.filter(lector=user)
        continuar = True
        for i in prestamos_usuario:
            if i.Fecha_devolucion == None and i.bibliotecario_entrega == None:
                messages.error(request, 'No puede pedir libros si tiene un préstamo activo/libro sin devolver')
                limpiar_articulo(request)
                continuar = False
                break

            else:
                continuar = True
        if continuar:
            prestamo = Pedido(request).realizar()
            for i in prestamo:
                id_ejemplar = prestamo[i]['ejemplar_id']

        #user = request.user
            nuevo_prestamo = Prestamo(Limite_entrega=(datetime.date.today() + datetime.timedelta(days=3)),
            ejemplar=Ejemplar.objects.get(id=id_ejemplar), lector=user)
            nuevo_prestamo.save()
            actualizar_ejemplar(id_ejemplar)
            messages.error(request, 'Pedido realizado')
        limpiar_articulo(request)
        return redirect("index_lector")

@user_passes_test(es_bibliotecario)
def entregado(request, id):
        user = request.user
        prestamo = Prestamo.objects.filter(id=id)
        prestamo.update(Fecha_entrega=datetime.date.today(), Limite_devolucion=(datetime.date.today() + datetime.timedelta(days=7)), bibliotecario_entrega=user)
        return redirect('index_prestamo')

def recibido(request, id):
    user = request.user
    prestamo = Prestamo.objects.filter(id=id)
    prestamo.update(Fecha_devolucion=datetime.date.today(), bibliotecario_recibe=user)
    id_ejemplar = prestamo[0].ejemplar.id
    ejemplar = Ejemplar.objects.filter(id=id_ejemplar)
    ejemplar.update(disponible=True)
    return redirect("index_prestamo")
