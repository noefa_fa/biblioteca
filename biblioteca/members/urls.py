from xml.dom.minidom import Document
from django.urls import path
from . import views

from members import pedido
from django.conf import settings
from django.contrib.staticfiles.urls import static

urlpatterns = [

    path('registration/', views.Registro, name = 'registro'),

    path('bibliotecario/libros/', views.index, name='index'),
    path('bibliotecario/libros/add/', views.add, name='add'),
    path('bibliotecario/libros/delete/<int:id>', views.delete, name='delete'),
    path('bibliotecario/libros/update/<int:id>', views.update, name='update'),
    path('bibliotecario/grafico/', views.grafico, name='grafico'),
    #path('grafico/csv/<queryset:a_csv>', views.a_csv, name="exportCSV"),

    path('bibliotecario/autor/', views.index_autor, name='index_autor'),
    path('bibliotecario/autor/add/', views.add_autor, name='add_autor'),
    path('bibliotecario/autor/delete/<int:id>', views.delete_autor, name='delete_autor'),
    path('bibliotecario/autor/update/<int:id>', views.update_autor, name='update_autor'),

    path('bibliotecario/genero/', views.index_genero, name='index_genero'),
    path('bibliotecario/genero/add/', views.add_genero, name='add_genero'),
    path('bibliotecario/genero/delete/<int:id>', views.delete_genero, name='delete_genero'),
    path('bibliotecario/genero/update/<int:id>', views.update_genero, name='update_genero'),

    path('bibliotecario/editorial/', views.index_editorial, name='index_editorial'),
    path('bibliotecario/editorial/add/', views.add_editorial, name='add_editorial'),
    path('bibliotecario/editorial/delete/<int:id>', views.delete_editorial, name='delete_editorial'),
    path('bibliotecario/editorial/update/<int:id>', views.update_editorial, name='update_editorial'),

    path('bibliotecario/ejemplar/', views.index_ejemplar, name='index_ejemplar'),
    path('bibliotecario/ejemplar/add/', views.add_ejemplar, name='add_ejemplar'),
    path('bibliotecario/ejemplar/delete/<int:id>', views.delete_ejemplar, name='delete_ejemplar'),
    path('bibliotecario/ejemplar/update/<int:id>', views.update_ejemplar, name='update_ejemplar'),

    path('bibliotecario/prestamo/', views.index_prestamo, name='index_prestamo'),
    path('bibliotecario/prestamo/add/', views.add_prestamo, name='add_prestamo'),
    path('bibliotecario/prestamo/delete/<int:id>', views.delete_prestamo, name='delete_prestamo'),
    path('bibliotecario/prestamo/update/<int:id>', views.update_prestamo, name='update_prestamo'),
    path('bibliotecario/prestamo/entregado/<int:id>', views.entregado, name='entregado_'),
    path('bibliotecario/prestamo/recibido/<int:id>', views.recibido, name='recibido'),


    #path('lector/agregar/<int:id>/', views.agregar_articulo, name="agregar"),
    path('eliminar/<int:id>/', views.eliminar_articulo, name="eliminar"),
    path('restar/<int:id>/', views.restar_articulo, name="restar"),
    path('limpiar', views.limpiar_articulo, name="limpiar"),
    path('guardar/', views.guardar_prestamo, name="guardar"),

    path('lector/catalogo/', views.index_lector, name='index_lector'),
    path('lector/agregar/<int:id>/', views.agregar_articulo, name="agregar"),
    path('lector/historial/', views.historial, name='historial'),



]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
