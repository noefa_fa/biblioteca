from django.contrib import messages

class Pedido:
    # Carrito lleno
    def __init__(self, request):
        self.request = request
        self.session = request.session
        prestamo = self.session.get("prestamo")
        if not prestamo:
            self.session["prestamo"] = {}
            self.prestamo = self.session["prestamo"]
        # vacio
        else:
            self.prestamo = prestamo
    # Crrito agregar
    def agregar(self, libro, ejemplar):
        if self.prestamo != {}:
            return False
        else:
            id = str(libro.id)
            if id not in self.prestamo.keys():
                self.prestamo[id]={
                    "libro_id": libro.id,
                    "titulo": libro.titulo,
                    "ejemplar_id": ejemplar.id,
                    #"acumulado": libro.precio,
                    "cantidad": 1,
                    }
                # producto ya existe
            elif id in self.prestamo.keys():
                self.prestamo[id]["cantidad"] += 1

            elif self.prestamo != None:
                messages.errors('No puede solicitar más de un libro')
                #self.prestamo[id]["acumulado"] += libro.precio
            self.guardar_prestamo()

    # guardar prestamo
    def guardar_prestamo(self):
        self.session["prestamo"] = self.prestamo
        self.session.modified = True

    # eliminar del prestamo
    def eliminar(self, libro):
        id = str(libro.id)
        if id in self.prestamo:
            del self.prestamo[id]
            self.guardar_prestamo()

    # restar
    def restar(self, libro):
        id = str(libro.id)
        if id in self.prestamo.keys():
            self.prestamo[id]["cantidad"] -= 1
            #self.prestamo[id]["acumulado"] -= libro.precio
            if self.prestamo[id]["cantidad"] <= 0: self.eliminar(libro)
            self.guardar_prestamo()

    # limpiar prestamo
    def limpiar(self):
        self.session["prestamo"] = {}
        self.session.modified = True

    def realizar(self):
        return self.prestamo
